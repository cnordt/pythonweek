import random
print("Hello World")
#This a comment
""" this is a doc string 
this can go on many lines
"""
#I would
#have to do this everytime I wanted a comment

#Pip our command tool for installing and managing python packages, like those found in the Python Package Index(PyPI)
#PyPI is a online repository of software for the python language
#if 5>2:
    #print( "five is less than two")
        #print("I can't do this")
print("Hello")
str1 = "Hello \
World"
str2 = """ Hello,
 World"""
print(str1)
print(str2)

int1= 1 + 2+ 3 \
    +4 + 5
print(int1)

x = 5
print(x)
print(type(x))
x = "Hello"
print(x)
print(type(x))

y = 5
#print(x + y) cannot concat strings and ints
y = str(y)
print(x + y)
#Casting, you can cast from one data type to another user str() int() float()
#STrings can use single quotes or double quotes
str3 = "This s my quote, 'Hello World.'."
print(str3)
# Python is case-sensetive
a = 5
A = "Hello"
print(a)
print(A)
#To see memory id
print(id(a))
print(id(A))
#Variable names but start with either a letter or an underscore, they cannot start with a number, can only contain alph-numeric characters and __
x=y=z = "Bob"
print(x)
print(y)
print(z)

x,y,z = 'Orange', 'Red', "Blue"
print(x)
print(y)
print(z)

#Unpacking a collection

colors = ["Red", "Pink", "Black"]
x,y,z = colors
print(x)
print(y)
print(z)

#Variable scopes, global and local
#global scope is a variable created outside of a function
#local is going to be based off of code blocks/ functions
#Global variables can be used both nside and out of functions

def myfunc():
    x = 6
    print(x)

myfunc()
print(x)

#We can create global variables inside of a function by using the global keyword

def myfunc2():
    global x
    x = 7
    print(x)

myfunc2()
print(x)

#Data types in Python
""" 
Text types: str
Numeric types: int, float, complex
Sequence types: list, tuple, range
Mapping types: dict
Set types: set, frozen set
Boolean: bool
Binary Types: bytes, bytearray, memoryview
to get the data type of a variable use the type()
set((1,2,3,4,5,6))
bytes(5)
"""
x = 1  #int
y = 1.1   #float
z = 1j  #complex
print(type(x))
print(type(y))
print(type(z))
#type conversion
a = float(x)
print(a)
b = int(y)
print(b)
c = complex(x)
print(c)

print(random.randrange(1,10))

#Strings
#Strings are arrays
a = "Hello, Wor,ld    "
print(a[1])
# Looping through a string
for x in a:
    print(x)
# length of a string
print(len(a))
#check to see if a phrase or character is in your string
print("lo" in a)
print("lol" in a)
print("lol" not in a)
#Slicing Strings
print(a[2:7])
print(a[:7])
print(a[2:])
#Negative indexing
print(a[-5:-2])
print(a[-2:-5])
#String functions
print(a.upper())
print(a.lower())
print(a.strip()) #strips whitespace at begining and end of a string
print(id(a.replace("Hello", "J")))
print(id(a))
print(a.split(","))# splits the str based off of a specified separator

#Concat
a = "Hello"
b = "World"
print(a+b)
c = a + ", " + b
print(c)

# in order to add integers into strings we must format our strings using the format function
age = 39
words = "My name is Bob and I am {1} years old and I am {0} inches tall"
#print(words.format(age))
height = 68
print(words.format(age,height))

#Escape Characters
str1 = "Hello I really like to use \"quotes\""
print(str1)
# to escape the escape character \\. for a new line \n, carriage return \r, tab \t
"""
capitalize()	Converts the first character to upper case
casefold()	Converts string into lower case
center()	Returns a centered string
count()	Returns the number of times a specified value occurs in a string
encode()	Returns an encoded version of the string
endswith()	Returns true if the string ends with the specified value
expandtabs()	Sets the tab size of the string
find()	Searches the string for a specified value and returns the position of where it was found
format()	Formats specified values in a string
format_map()	Formats specified values in a string
index()	Searches the string for a specified value and returns the position of where it was found
isalnum()	Returns True if all characters in the string are alphanumeric
isalpha()	Returns True if all characters in the string are in the alphabet
isdecimal()	Returns True if all characters in the string are decimals
isdigit()	Returns True if all characters in the string are digits
isidentifier()	Returns True if the string is an identifier
islower()	Returns True if all characters in the string are lower case
isnumeric()	Returns True if all characters in the string are numeric
isprintable()	Returns True if all characters in the string are printable
isspace()	Returns True if all characters in the string are whitespaces
istitle()	Returns True if the string follows the rules of a title
isupper()	Returns True if all characters in the string are upper case
join()	Joins the elements of an iterable to the end of the string
ljust()	Returns a left justified version of the string
lower()	Converts a string into lower case
lstrip()	Returns a left trim version of the string
maketrans()	Returns a translation table to be used in translations
partition()	Returns a tuple where the string is parted into three parts
replace()	Returns a string where a specified value is replaced with a specified value
rfind()	Searches the string for a specified value and returns the last position of where it was found
rindex()	Searches the string for a specified value and returns the last position of where it was found
rjust()	Returns a right justified version of the string
rpartition()	Returns a tuple where the string is parted into three parts
rsplit()	Splits the string at the specified separator, and returns a list
rstrip()	Returns a right trim version of the string
split()	Splits the string at the specified separator, and returns a list
splitlines()	Splits the string at line breaks and returns a list
startswith()	Returns true if the string starts with the specified value
strip()	Returns a trimmed version of the string
swapcase()	Swaps cases, lower case becomes upper case and vice versa
title()	Converts the first character of each word to upper case
translate()	Returns a translated string
upper()	Converts a string into upper case
zfill()	Fills the string with a specified number of 0 values at the beginning
"""
#Truthy anf false values
#most values are true
#Any sting that isn't empty will be true, All numbers except 0 are true, all lists, tuple,set, and dcitionaries are true except empty ones, None is also false
b = bool(None)
print(b)
b= bool([1,2,3])
print(b)
b = bool([])
print(b)