# if
a = 6
b =6

if b>a:
    print("b is greater than a")
elif a>b:
    print("a is greater than b")
else:
    print("a and b are equals")

#short hand
if a > b: print ("a is greater than b") 

print("a") if a > b else print("b")
# you cannot have an empty code block, so iif you want code for readibility you have to use the pass statement
if a == b:
    pass
else:
    print("They are not equal")

#While
i = 0
while i< 10:
    print(i)
    i += 1

j = 1
while j<10:
    if j == 9:
        break
    else:
        j += 1

j = 1
while j<10:
    if j == 9:
        j+=1
        continue
    else:
        print(j)
    j+=1
else:
    print("j is no longer  less than 10")

fruits = ["Apple", "Berry", "Grape"]
for x in fruits:
    print(x)

#Range function
for i in range(6):
    print(i)
print()    
for i in range(2,6):
    print(i)
print()    
for i in range(2,20,2):
    print(i)
else:
    print("We counted to 20 by 2s")
print()    
for i in range(20,2,-4):
    print(i)