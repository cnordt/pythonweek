#Iterator is an object that can be iterated upon
#An interator is any object that has the iterator protocol, __iter__() and the __next__()
mylist = [1,2,"three",4,5]
myit = iter(mylist)
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))

class MyIterator:
    def __iter__(self):
        self.i = 1
        return self
    def __next__(self):
        if self.i <=20:
            x = self.i
            self.i += 1
            return x
        else:
            raise StopIteration

myit = MyIterator()
it = iter(myit)
#print(next(it))
#print(next(it))
#print(next(it))
#print(next(it))
for x in it:
    print(x)
bad = 5