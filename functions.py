#a function is a block of code that runs when it has been called, they are not attached to an object
#functions are defined using the def keyword
def my_func():
    print("Welcome to my function")

my_func()

def my_name(fname, lname):
    print("My name is " + fname + " " +lname)

my_name("Caroline", "Nordt")
#my_name("Caroline","Rebecca","Nordt")

#*args, this allows for an unknown amount of arguments, it will create a tuple of the passed in parameters
def my_var_args(*nums):
    for i in nums:
        print(i)
my_var_args(1,2,3,4,5,6,7,8,9,10)
my_var_args("one", "two", "Three", 10 ,2 )

#keyword arguments, you pass in paramter use this syntax key=value
my_name(lname="Nordt", fname = "Caroline")

#if you don't know how many keyword arguments you might have you can use **kwargs, this will give you a dictionary of the arguments
def my_kids(**kids):
    if(kids["lname"]==None and kids["species"] == None):
        print("Their name is " +kids["fname"])
    else:
        print("My " + kids["species"] + "'s name is " +kids["fname"]+ " " + kids["lname"])

my_kids(fname="Stella", lname = "Nordt", species="Dog")

#Default parameter, allows you to assign a default value for a argument if none is given by the user
def my_country(name = None):
    if name is not None:
        print("I am from " +name)
    else:
        print("The country was not provided")
my_country("Norway")
my_country("Chile")
my_country()

#Returning
def my_return(a,b):
    return a+b
print(my_return(2,2))

#Pass keyword
def my_func2():
    pass
my_func2()
def my_fib(n):
    if n==0:
        return 0
    elif n == 1:
        return 1
    else:
        return my_fib(n-1) + my_fib(n-2)

print(my_fib(5))


#Lambda function: is a small anonymous function syntax -> lanbda args: expression

x = lambda a, b: a+b
print(x(2,2))

def myfunc(n):
    return lambda a: a*n

mydouble = myfunc(2)
print(mydouble(4))

mytriple = myfunc(3)
print(mytriple(3))

