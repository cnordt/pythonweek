#Test files must start with test_*.py or *_test.py
#All test functions must have test* in the function name
import calc
import pytest

def test_add():
    num1 = 1
    num2 = 2
    num3 =3
    num4 =4
    assert calc.add(num1,num2) ==3
    assert calc.add(num1,num2,num3,num4) == 10
    assert calc.add(num1,num2) != -1
    assert calc.add(num2,num4) != 8
    assert calc.add(num4,num2) !=2

def test_add_input():
    with pytest.raises(TypeError) as excinfo:
        calc.add("hello", 4)