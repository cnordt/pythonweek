import psycopg2

#Create the connection string
conn = psycopg2.connect(
    host = "localhost",
    database = "postgres",
    user ="postgres",
    password = "p4ssw0rd"
)


#How to query information
def getFoods():
    sql ="SELECT * FROM food"
    cur = conn.cursor()
    cur.execute(sql)
    print("Number of foods", cur.rowcount)
    row = cur.fetchone()
    while row is not None:
        print(row)
        row = cur.fetchone()
def getListFoods():
    sql ="SELECT * FROM food"
    cur = conn.cursor()
    cur.execute(sql)
    print("Number of foods", cur.rowcount)
    rows = cur.fetchall()
    for row in rows:
        print(row)
    cur.close()

def addFood(name, cal):
    sql = "INSERT INTO food(name,calories) VALUES(%s,%s)"
    cur = conn.cursor()
    cur.execute(sql,(name,cal))
    conn.commit()
    cur.close()

def addManyFoods(food_list):
    sql = "INSERT INTO food(name,calories) VALUES(%s,%s)"
    cur = conn.cursor()
    cur.executemany(sql,food_list)
    conn.commit()
    cur.close()

def updateFood(name,cal):
    sql = "UPDATE food SET calories = %s WHERE name = %s"
    cur = conn.cursor()
    cur.execute(sql,(cal,name))
    conn.commit()
    cur.close()
#addFood("ice cream", 350)
#food_list=[("pineapple",100),("Hot Chocolate", 200)]
#addManyFoods(food_list)
#How to call functions in python
#cur.callproc('function_name',(value1,value2))
#cur.execute("Select * From function_name(%s,%s)",(value1,value2))

#Stored procedures
#cur.execute("CALL sp_name(%s,%s)", (value1,value2))

updateFood("pineapple", 50)
getListFoods()
