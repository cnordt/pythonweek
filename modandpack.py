"""
modules typcally define separate namespaces, this helps avoid collisions between identifiers
namespaces are the structures used to organize the symbolc names assigned to our objects and what they reference 
in python there are 4 namespace types: Built in, Global, Enclosed, and Local
Built-in: contains all of the names of all of Pythhon's built in objects
global: contains any objects defined at the level of the main program
Local:it the namespace created when a function starts until it terminates, or if a function s enclosed within another
Enclosing: the namespace of a function that encloses another
Scope goes from: l<e<g<b

to access different modules we use the inport statement -> import module name
we can also import using an alias -> import module as alias
import specific module members -> from module import members

Packages:
allow for hierarchical structuring of the module namespace using dot notation
think of packages lke folders
importing from a package
import pkg.mod1, pkg.mod2

"""
#import random
#import random as r
#from random import randrange
import random
import OOP
#print(dir(__builtins__))
#k = 7
#def main():
#    x = "hello world" #enclosing namespace

#    def g():
#        b = 5
#        print("I'm trapped") #local namespace

#print(randrange(1,20))
d = OOP.Dog()
##dir() returns a list of defined names in a namespace
print(dir(random))
