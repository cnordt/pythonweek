import sendthedata
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017")
mydb = myclient["mymessages"]
mycol = mydb["Kafka"]

mess = input("What would you like to send?")
sendthedata.sendMess(mess)

for x in mycol.find():
    print(x["message"])
