from abc import ABC, abstractmethod
#Inheritance

class Parent:
    def __init__(self, fname, lname):
        self._firstname = fname
        self.lastname = lname
    
    def printname(self):
        print(self._firstname, self.lastname)

z = Parent("Caroline","Nordt")
print(z._firstname)
z.printname()

class Child(Parent):
    #The childs init is overriding the parents, polymorphism
    def __init__(self, nickname):
        self.nick = nickname
        super().__init__("Caroline","Nordt")
        print(self.nick,self._firstname,self.lastname)
    def printname(self):
        super().printname()
        print(self.nick)
    def bark(self):
        print("woof")

x = Child("Stella")
x.printname()
x.bark()

##Encapsulation
#Protected variables and methods will be prepended with _
#Private variables and methods will be prepended with __

class Computer:
    def __init__(self):
        self.__maxprice = 1000
    def sell(self):
        print("The computer is priced to sell at {} dollars".format(self.__maxprice))
    def setMaxPrice(self,price):
        self.__maxprice = price

c =Computer()
c.maxprice = 900
print(c.sell())
c.setMaxPrice(900)
print(c.sell())

#Abstraction in base python   is consider to be fulflled by using proper encapsulation
# Python comes with the abc module, this provides functionality for abstract classes
# to create an abstract class we must import the abc module and then define the class with abc.ABC and define the abstract methods with
# the @abstractmethod decorator 
class Animal(ABC):
    @abstractmethod
    def move(self):
        print("The Animal Moved")
#a=Animal() cannot instantiate an abstract class
class Dog(Animal):
    def move(self):
        super().move()
        print("The dog ran")
d = Dog()
d.move()