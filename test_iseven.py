import calc
import iseven
import pytest
from unittest import mock

def test_even_or_odd(mocker):
    mocker.patch('calc.add', return_value = 6 )
    assert iseven.even_or_odd(3,2) == 'even'
    #mocker.assert_called_with(3,2)

def test_params():
    with mock.patch('calc.add') as patched_function:
        iseven.even_or_odd(2,4)
    patched_function.assert_called_with(2,4)