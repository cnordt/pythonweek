import math

#min() and max() 

x = min(1,2,3,4)
y = max(1,2,3,4)
print(x,y)

#abs() this gives you the absolute value of a number
print(abs(-564))

#pow(x,y) x to the power of y

#the math module brings in further mathematical operations like math.sqrt() math.ceil(), math.floor(), math.pi