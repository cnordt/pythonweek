class MyClass:
    #This belongs to the class
    x = "Hello World"
    #The init method, it is called when the class is innitiated
    def __init__(self, name, age):
        #What I pass in as arguments for my init() will be the instances properites 
        self.name = name
        self.age = age
    def myfunc(self):
        print("Hello my name is " + self.name)
    #The self paramter is a reference to the current instance of the class, it s used to access variables that belong to the class
    #it has to be the first parameter of any function within a class

mc = MyClass("Caroline" , 24)
print(mc.x +" I am {} and I am {} years old".format(mc.name,mc.age))
mc.myfunc()

mc.age = 63
print(mc.age)
del mc.age
#print(mc.age)
del mc
#print(mc)

class Person:
    pass

