#How do we create and utilize date in Python
import datetime
#print(dir(datetime))
# to see the current date
x =datetime.datetime.now()
print(x)
#get year and weekday
print(x.year)
print(x.strftime("%A"))
#create a date
day = datetime.datetime(2021, 2,9)
print(day)

#strftime(), this method converts the dates into readable strings
"""
%a - weeday short version
%A - weekday full version
%w - weekday as number
%d - day of the month
%b - The name of the month short
%B name of the month
%m	Month as a number 01-12	12	
%y	Year, short version, without century	18	
%Y	Year, full version	2018	
%H	Hour 00-23	17	
%I	Hour 00-12	05	
%p	AM/PM	PM	
%M	Minute 00-59	41	
%S	Second 00-59	08	
%f	Microsecond 000000-999999	548513	
%z	UTC offset	+0100	
%Z	Timezone	CST	
%j	Day number of year 001-366	365	
%U	Week number of year, Sunday as the first day of week, 00-53	52	
%W	Week number of year, Monday as the first day of week, 00-53	52	
%c	Local version of date and time	Mon Dec 31 17:41:00 2018	
%x	Local version of date	12/31/18	
%X	Local version of time	17:41:00	
%%	A % character	%	
%G	ISO 8601 year	2018	
%u	ISO 8601 weekday (1-7)	1	
%V	ISO 8601 weeknumber (01-53)	01
"""
print(x.strftime("%B"), x.strftime("%A"), x.strftime("%d"), x.strftime("%Y"))