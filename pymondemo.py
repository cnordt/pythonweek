import pymongo

#this is establishing the connection with the database
myclient = pymongo.MongoClient("mongodb://localhost:27017")
#this is selecting the db to connect with, if it doesn't exisit it will eventually create it, I need a collection and at least one document/record first
mydb = myclient["mydemo"]
#To create/reference a collection
mycol = mydb["Jedi"]
#To insert into my collection
#my_Jedi = {"name":"Mace Windu", "lightsaber":"purple"}
my_Jedi = {"name":"Obi Wan", "lightsaber":"Blue"}
#this method will return a InsertOneResult
#x = mycol.insert_one(my_Jedi)
#print(x.inserted_id)

#To insert multiplle
##jedis = [{"_id":"1","name":"Anakin Skywalker", "lightsaber": "Red"},
 #      {"_id":"2","name" : "Luke Skywalker", "lightsaber" : "Green"}]
#x= mycol.insert_many(jedis)
#print(x.inserted_ids)
#selcting data back
#find_one() returns the first occurance in this selection
x = mycol.find_one()
print(x)
#Find all find() returns all occurrences in the selection
y = mycol.find()
for z in y:
    print(z)

for x in mycol.find({},{ "_id": 0 , "name":0 }):
    print(x)

#Querying
myquery = {"lightsaber":"purple"}
myPurple = mycol.find(myquery)
for x in myPurple:
    print(x)

#Advanced queries
myq2 = {"name" :{"$gt":"M"}}
myJedi = mycol.find(myq2)
for jed in myJedi:
    print(jed)
print()
#Filter fith regex
myq3 = {"name":{"$regex":"^M"}}
mace = mycol.find(myq3)
for x in mace:
    print(x)
print()
#sort(field,direction) will sort the results in ascending or descending order
myJedis = mycol.find().sort("lightsaber",-1)
for x in myJedis:
    print(x)

#delete_one will delete the first occurance of that record
myq4 = {"name" : {"$regex": "^A"}}
#mycol.delete_one(myq4)
#myJedis = mycol.find().sort("lightsaber",-1)
#for x in myJedis:
#   print(x)

#Delete many
##print(x.deleted_count, " he has gone to the dark side")

#if I want to see the collections in my database I can use the list_collection_names() on my database
#mycol.drop()

#Update_one will update the first occurance of the record update_many will update all occurance

#myquery = {"name":{"$regex":"^A"}}
#newvalues = {"$set":{"name":"Darth Vader"}}
#mycol.update_one(myquery,newvalues)
for x in mycol.find().limit(1):
    print(x)
print(mydb.list_collection_names())
#to see all databases available use the list_database_names()
print(myclient.list_database_names())