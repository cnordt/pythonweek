#try-block that lets us test risky code
#except- block that lets you handle the error
#finally- executes regardless of the try, except result
import random
try:
    print(x)
except:
    print("An error occured")

#Can look for specific exceptions
try:
    print(x)
except NameError:
    print("Var x not defined")
except:
    print("Something else went wrong")

#What if the try block doesn't through an error, I can use an else
try:
    print("hello")
    #print(x)
except:
    print("Something went wrong")
else:
    print("Nothing went wrong")
finally:
    print("Done")

#How do I throw an exception, we will use the word raise
x = -1
#if x< 0:
    #raise Exception("No negative numbers please")
#User input
#y = float(input("Please input something: "))
#print(y)

#String formating
#txt = "The price of this {fruit} is {0:.2f} dollars"
#print(txt.format(y,fruit="apple"))

correct = random.randrange(0,1000)
print(correct)
ans  = correct +1
while(ans!=correct):
    ans = int(input("Please input your guess?"))
    if ans == correct:
        print("Congrats you won the game")
    elif ans > correct:
        print("Guess lower")
    else:
        print("Guess Higher")
