import json

#this module provides functionalitiy for utilizing jsons

#Convert from json to python, you can use the json.loads()
x =  '{ "name":"John", "age":30, "city":"New York"}'
y = json.loads(x)
print(y["age"])
print(type(y))

#Convert from python to Json json.dumps(), dict, list, tuple, strings, int, float, True, False, None

z = json.dumps(y, indent = 4, separators=(", ",": "), sort_keys=True)
print(type(z))
print(z)