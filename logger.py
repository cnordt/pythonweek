#we import the logging module
import logging
"""
Debug
Info
Warn
Error
Critical
"""
#The module gives you a default logger
logger = logging.getLogger("My Logger")

#create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('file.log')
c_handler.setLevel(logging.DEBUG)
f_handler.setLevel(logging.WARNING)


#add in formating
c_format = logging.Formatter('%(name)s -%(levelname)s - %(message)s')
f_format = logging.Formatter('%(name)s -%(levelname)s - %(message)s - %(asctime)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

#attatch them to mt logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)

#logging.basicConfig(level = logging.DEBUG, filename="app.log", filemode="a", format="%(process)s - %(name)s - %(levelname)s - %(message)s %(asctime)s")
name = "bob"
logger.debug("This is a debug")
logger.info(f'{name} logged out')
logger.warning("This is a warning")
logger.error("This is an error")
logger.critical("This is critical")

logger.debug("did this get logged")
try:
    g = "hello" +3
except Exception as e:
    #logging.error("Exception occurred ", exc_info = True)
    logger.exception("exception occurred")
"""
Commonly used classes from logging
Logger: this is used to call the functions
LogRecord: contains all info about an event being logged
Handler: send it to its destination
Formatter: specify the formate of the output
"""

