""" 
Operators in Python
Arithmetic
+:addition
-: substraction
*:multiplication
/: Division
//: Integer or floor division
%: modulus
**: exponentiation

Assignment
=: assignment
+=: x+=3 -> x = x +3
-=: x -=3
*=
/=
//= x //= 3 -> x = x//3
%=
**=
&= x = x&3
|=
^=
>>=
<<=

Comparison
==: equality
!=: not equal
>: greater than
<: less than
>=: greater than or equal
<=: less than or equal

Logical
and: return true if both statements are true
or: returns true if one of the statements is true
not: reverses the result, returns False if the result is true

Identity
is: returns true if they are the same variable
is not: returns true if they are not the same variable

Membership Operators
in: returns true if a sequence with the specified value is present in the object
not in: which returns true if the sequences is not in the object

Bitwise
&: and
|: OR
^: XOR
~: Not
<<: Zero fill left shift
>>: Signed right shift
"""

#Lists, they are created using [], are ordered, changeable, and allow duplicate values, they are indexed
my_list = [ 1,2,3,4,5, "Potato"] #you are not restricted by data type
print(my_list)
#the len() will tell you the length of your list
#The list constructor list()
thislist = list(("hello", "world", 3, 42,"bob"))
print(thislist)
print(thislist[1])
#Negative indexing, the left most index will be denoted by a -1  the one before that -2, and so on
print(thislist[4])
print(thislist[-1])
print(thislist[2:4])
print(thislist[:3])
print(thislist[-4:-1])
print(42 in thislist)
thislist[4] = 65
print(thislist)
print(len(thislist))
thislist[1:2] = ["Goodbye", "World"]
print(thislist)
print(len(thislist))
# the insert method adds a new item to the list without replacing the existing
thislist.insert(2,90)
print(thislist)
#APPEND ADDS A NEW ITEM TO THE END OF A LIST
thislist.append("bob")
print(thislist)
thislist.extend(my_list)
print(thislist)
thislist.remove("Potato")
print(thislist)
#pop() will remove a specific index, if an ndex is not specified it will remove the last item
thislist.pop(3)
print(thislist)
#the del keyword del removes a specific index
del thislist[0]
print(thislist)
#del thislist this will delete the list entierly
print(thislist)
#the clear method will remove all items from the list
#print(thislist.clear())
#the sort() will sort the list alphanumerically

num_list= [1,5,7,3,4,9,2,1]
num_list.sort(reverse=True)
print(num_list)
#Sort based on my own logic the sort() can take in the parameter key = func
def myfunc(n):
    return abs(n-5)
print(num_list.sort(key = myfunc))
print(num_list)
#if sorting lists of words you can make it case insensitive by passing key = str.lower
#reberse the order of a list you can use the reverse()
#if I do list2=list1
list1 = [1,2,3,4,5]
list2 = list1
print(id(list1))
print(id(list2))
#What if I wanted two copies of the same list not stored at the same place in memory
#Well you can use the copy fuction
#list2 = list1.copy()
#print(id(list2))
list2.append(4)
print(list1)
#Other ways to join lists you can do concat +, you can use append(), extend()

#Tuples
#these are used to store multiple items ina single variable
#This is ordered and unchangeable
#Tuplles are created useing ()
my_tuple = ("Hello", "World")
print(my_tuple)
print(my_tuple[0])
#to create a tuple with only one item it must have a comma after the item
my_one=(1,)
print(type(my_one))
#To update tuples you can first convert it to a list perform your changes and then convert it back
my_change = list(my_tuple)
my_change.append("Goodbye")
my_tuple = tuple(my_change)
print(my_tuple)
#When creating a tuple we assign values to it, this is called packing, unpacking is extracting values
#Unpacking
fruits = ("kiwi", "orange", "apple", "plum")
x,y,z,a = fruits
print(x)
print(y)
print(z)
print(a)
x,y,*z = fruits
print(x)
print(y)
print(z)
x,*y,z = fruits
print(x)
print(y)
print(z)
#Joiining tuples
tuple2 = my_tuple + fruits
print(tuple2)
more_fruits = fruits * 5
print(more_fruits)


#Sets
#are unordered and unindexed, sets are unchangeable, do not allow duplcate values
#sets are created with {}
my_set = {"apple", "kiwi", "cherry","apple"}
print(my_set)
for x in my_set:
    print(x)
#add()
my_set.add("banana")
print(my_set)
new_set = {"Boat", "Car", "Plane"}
my_set.update(new_set)
print(my_set)
#removing items
my_set.remove("banana")
print(my_set)
my_set.discard("banana")
x = my_set.pop()
print(x)
#Union will return a new set with items from two sets
set3 = my_set.union(new_set)
print(set3)
#Only want to keep what exisits in both lists
set4 = {"kiwi", "Car", "Boat"}
set4.intersection_update(my_set)
print(set4)

set5 = set3.intersection(set4)
print(set5)

x = {"Apple", "Berry", "Grape"}
y = {"Car", "Berry"}
#x.symmetric_difference_update(y)
z = x.symmetric_difference(y)
print(z)

#Dicitonaries
#They are key value pairs, ordered, changeable, and does not allow duplicates
my_car = {
    "brand" : "Subaru",
    "model" : "Outback",
    "year" : 2013
}
print(my_car)
print(my_car["year"])
my_car2 = {
    "brand" : "Subaru",
    "model" : "Outback",
    "year" : 2013,
    "colors" : ["red", "silver", "black"]
    #"year" : 2019
}
print(my_car2)
#get method
print(my_car.get("model"))
x= my_car.keys()
my_car["colors"] = "Red"
print(x)
print(my_car.values())
y = my_car.items()
print(y)
#Update values
my_car.update({"interior_colors" : "tan"})
print(my_car)
my_car.pop("interior_colors")
print(my_car)
new_car = dict(my_car)
print(id(my_car))
print(id(new_car))
my_cars = {
    "car1" : my_car,
    "car2" : my_car2,
    "car3" : new_car
}
print(my_cars)