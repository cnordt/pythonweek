#Reading and writing to files
#Open() takes teo parameters the filename and mode
import os
print(dir(os))
"""
r-read, causes an error if the file doesn't exist
a - append, opens the file or creates it if the file isn't there and adds on addition text
w - Write, open file for writing or create if the file doesn't exist, but this will override any existing information in the file
x - Create errors if the file already exists

to specify if text or binary we can append t or b to our mode
"""

#Create
#f = open("example.txt", "x")

#append
#file = open("example.txt","a")
#for count in range(0,20):
#    file.write("This is my example text {} \n".format(count))

#Read
#file = open("example.txt")
#print(file.read(255))
#print(file.readline())
#for x in file:
#    print(x)
#file.close()

#Deleting requires us to use the os module
if os.path.exists("example.txt"):
    os.remove("example.txt")
else:
    print("the file doesn't exist")